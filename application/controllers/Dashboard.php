<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends PM_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('member_model');
	}

	public function index()
	{
		$view_params = array();
		$view_params['member_count'] = $this->PM_Model->get_count_value($this->user_table, array('type'=>'MEMBER'));
		$view_params['approved_count'] = $this->PM_Model->get_count_value($this->user_table, array('type'=>'MEMBER', 'is_active'=>'APPROVED'));
		$view_params['pending_count'] = $this->PM_Model->get_count_value($this->user_table, array('type'=>'MEMBER', 'is_active'=>'PENDING'));
		$view_params['partner_count'] = count($this->member_model->get_relationship($this->my_id, 'PARTNER'));
		$view_params['children_count'] = count($this->member_model->get_relationship($this->my_id, 'CHILDREN'));
		$view_params['parent_count'] = count($this->member_model->get_relationship($this->my_id, 'PARENT'));

		$view_params['grandparent_count'] = count($this->member_model->get_relationship($this->my_id, 'GRANDPARENT'));
		$this->load_view('dashboard', $view_params);
	}
}
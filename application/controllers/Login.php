<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends PM_Controller {

    public function index()
    {
        if ($this->isLogin) {
            redirect('dashboard');
        }

        $this->load->view('login/index');
        $this->load->view('login/jslink');
    }

    public function ajax_login() {
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        $response = array();
        $response['state'] = 'failed';

        if (empty($email) || empty($password)) {
            $response['message'] = 'Email or Password is Empty.';
            return $this->load_json($response);
        }

        $where = array();
        $where['email'] = $email;
        $where['password'] = sha1($password);

        $user = $this->PM_Model->get_info_with_where('users', $where);

        if (empty($user)) {
            $response['message'] = 'Wrong Email or Password.';
            return $this->load_json($response);
        } 

        if($user['type'] == 'MEMBER' && $user['is_active'] == 'PENDING') {
            $response['message'] = 'Your account was not approved.';
            $this->load_json($response);
            return;
        }

        $this->user_session_create($user);
        
        $response['state'] = 'success';
        $response['url'] = 'dashboard';

        $this->load_json($response);
        
    }

    public function user_session_create($user) {
        $this->session->set_userdata(array(
            'id' => $user['id'],
            'email' => $user['email'],
            'type' => $user['type'],
            'logged_in' => true
        ));
    }

    public function ajax_forget_pwd(){
        $email = $this->input->post('email');
        $user_info = array();

        $user_info = $this->PM_Model->get_info_with_where($this->user_table, array('email'=>$email));
        if(empty($user_info)) {
            $resp['state'] = 'failed';
            $resp['message'] = 'Your email does not exist';
            $this->load_json($resp);
            return;
        }

        if($user_info['is_active'] == 'PENDING') {
            $resp['state'] = 'failed';
            $resp['message'] = 'Your account was not approved.';
            $this->load_json($resp);
            return;
        }
        $random_password = uniqid($user_info['id'].date('s'));        
        //SMTP & mail configuration
        $this->load->library('email');
        $config = array(
            'protocol'  => 'smtp',
            'smtp_host' => 'smtp.gmail.com',
            'smtp_port' => 587,
            'smtp_user' => SMTP_URER,
            'smtp_pass' => SMTP_PASS,
            'mailtype'  => 'html',
            'charset'   => 'utf-8'
        );
        $this->email->initialize($config);
        $this->email->set_mailtype("html");
        $this->email->set_newline("\r\n");

        //Email content
        $htmlContent = '<h3>Your password successfully updated. </h3>
        <p>This is the new password :<strong> '.$random_password.'</strong></p>
        <p>Please try to login with this password.</p>
        <p>Regards.</p>';

        $this->email->to($email);
        $this->email->from(SMTP_URER,'Community');
        $this->email->subject('Password Reset');
        $this->email->message($htmlContent);

        //Send email
        if($this->email->send()) {
            $update['id'] = $user_info['id'];
            $update['password'] = sha1($random_password);
            $this->PM_Model->save($this->user_table, $update);

            $resp['state'] = 'success';
            $resp['message'] = 'We have just sent the new password to your email. Please check your email.';
            $this->load_json($resp);

        }
    }

    public function ajax_register(){

        $req = $this->input->post();
        
        $email = $req['email'];
        $resp = array();

        $user_info = $this->PM_Model->get_info_with_where($this->user_table, array('email'=>$email));

        if(!empty($user_info)) {
            $resp['state'] = 'failed';
            $resp['message'] = 'Your email already exist';
            $this->load_json($resp);
            return;
        }

        unset($req['rpassword']);
        $req['password'] = sha1($req['password']);

        $registered_id = $this->PM_Model->save($this->user_table, $req);

        $user = $this->PM_Model->get_info_with_where($this->user_table, array('id'=>$registered_id));

        $this->user_session_create($user);
        
        $resp['state'] = 'success';
        $resp['url'] = 'profile';
        $this->load_json($resp);

    }

    public function logout() {
        session_destroy();
        redirect('/');
    }
}

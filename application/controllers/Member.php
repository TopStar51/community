<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends PM_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('member_model');
    }

	public function index()
	{

        $view_params = array();
        $view_params['members'] = $this->PM_Model->get_list($this->user_table, 0, array('type'=>'MEMBER'));

        $this->load_js('assets/global/scripts/datatable.js');
        $this->load_js('assets/global/plugins/datatables/datatables.min.js');
        $this->load_js('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js');
        $this->load_css('assets/global/plugins/bootstrap-select/css/bootstrap-select.css');
        $this->load_js('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js');

        $this->load_view('member/management', $view_params);
	}

    public function ajax_get_members()
    {

        $input = $this->input->post();

        $where['type'] = 'MEMBER';

        if (isset($input['action']) && $input['action'] == 'filter' )
        {
            if ($input['first_name'] != '' )
                $where['first_name like'] = '%'.$input['first_name'].'%';
            if ($input['last_name'] != '' )
                $where['last_name like'] = '%'.$input['last_name'].'%';
            if ($input['birthday'] != '' )
                $where['birthday like'] = '%'.$input['birthday'].'%';
            if ($input['city'] != '' )
                $where['city like'] = '%'.$input['city'].'%';
            /*if ($input['state'] != '' )
                $where['state like'] = '%'.$input['state'].'%';
            if ($input['zip'] != '' )
                $where['zip like'] = '%'.$input['zip'].'%';*/
            if ($input['email'] != '' )
                $where['email like'] = '%'.$input['email'].'%';
            if ($input['head_of_household'] != '' )
                $where['head_of_household'] = $input['head_of_household'];
            if ($input['is_married'] != '' )
                $where['is_married'] = $input['is_married'];
            if ($input['has_child'] != '' )
                $where['has_child'] = $input['has_child'];
            if ($input['is_active'] != '' )
                $where['is_active'] = $input['is_active'];
        }

        $user_list = $this->PM_Model->get_list($this->user_table, 0, $where, 'created_at', 'DESC');

        $iTotalRecords = count($user_list);
        $iDisplayLength = $iTotalRecords;
        $iDisplayStart = 0;

        $records = array();
        $records["data"] = array();

        for( $i = $iDisplayStart; $i < $iDisplayLength; $i ++ )
        {
            $user_info = $user_list[$i];

            $status1 = $status2 = $status3 = "";
            
            if($user_info['head_of_household']) {
                $status1 = '<i class="fa fa-check"></i>';
            }
            if($user_info['is_married']) {
                $status2 = '<i class="fa fa-check"></i>';
            }
            if($user_info['has_child']) {
                $status3 = '<i class="fa fa-check"></i>';
            }

            if($user_info['is_active'] == "APPROVED") {
                $statusHtml = '<span class="label label-sm label-success"> Approved </span>';
            } else {
                $statusHtml = '<span class="label label-sm label-danger"> Pending </span>';
            }
            $records["data"][] = array(
                $user_info['first_name'].'<input type="hidden" class="user-first_name" value="'.$user_info['first_name'].'"/>',
                $user_info['last_name'].'<input type="hidden" class="user-last_name" value="'.$user_info['last_name'].'"/>',
                $user_info['birthday'].'<input type="hidden" class="user-birthday" value="'.$user_info['birthday'].'"/>',
                $user_info['address1'].'<input type="hidden" class="user-address1" value="'.$user_info['address1'].'"/>
                <input type="hidden" class="user-address2" value="'.$user_info['address2'].'"/>',
                '<input type="hidden" class="user-city" value="'.$user_info['city'].'"/>
                <input type="hidden" class="user-state" value="'.$user_info['state'].'"/>
                <input type="hidden" class="user-zip" value="'.$user_info['zip'].'"/>
                <input type="hidden" class="user-country" value="'.$user_info['country'].'"/>
                <input type="hidden" class="user-cell_phone" value="'.$user_info['cell_phone'].'"/>
                <input type="hidden" class="user-home_phone" value="'.$user_info['home_phone'].'"/>',
                $user_info['email'].'<input type="hidden" class="user-email" value="'.$user_info['email'].'"/>',
                '<span style="color:green;">'.$status1.'</span>'.'<input type="hidden" class="user-head_of_household" value="'.$user_info['head_of_household'].'"/>',
                '<span style="color:green;">'.$status2.'</span>'.'<input type="hidden" class="user-is_married" value="'.$user_info['is_married'].'"/>',
                '<span style="color:green;">'.$status3.'</span>'.'<input type="hidden" class="user-has_child" value="'.$user_info['has_child'].'"/>',
                $statusHtml.'<input type="hidden" class="user-is_active" value="'.$user_info['is_active'].'"/>',
                '<a href="javascript:;" class="btn btn-sm btn-outline btn-primary edit"><i class="fa fa-edit"></i>Edit</a>
                <a href="javascript:;" class="btn btn-sm btn-outline btn-danger delete"><i class="fa fa-remove"></i>Delete</a>
                <input type="hidden" class="user-gender" value="'.$user_info['gender'].'" />
                <input type="hidden" class="user-country" value="'.$user_info['country'].'" />
                <input type="hidden" class="user-id" value="'.$user_info['id'].'" />'
            );
        }

        $records['recordsTotal'] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo $this->load_json($records);
    }

    public function ajax_delete_member() {
        $input = $this->input->post();

        $this->PM_Model->delete_info($this->user_table, $input['id']);

        $response = array();
        $response['state'] = 'success';
        $this->load_json($response);
    }

	public function add(){

		$view_params = array();
		$view_params['members'] = $this->PM_Model->get_list($this->user_table, 0, array('type'=>'MEMBER'));
		$this->load_view('member/add', $view_params);
	}

    public function add_member(){
        $req = $this->input->post();

        $exist_member = $this->PM_Model->get_info_with_where($this->user_table, array('email'=>$req['email']));
        if(!empty($exist_member)) {
            $resp['state'] = 'failed';
            $resp['msg'] = 'This email already exists!';
            $this->load_json($resp);
            return;
        }
        $partner_id = $children_id = $parents_id = $grandparents_id = array();

        if(!empty($req['partner_id'])) $partner_id = $req['partner_id'];
        if(!empty($req['children_id'])) $children_id = $req['children_id'];
        if(!empty($req['parents_id'])) $parents_id = $req['parents_id'];
        if(!empty($req['grandparents_id'])) $grandparents_id = $req['grandparents_id'];

        unset($req['rpassword']);
        unset($req['partner_id']);
        unset($req['children_id']);
        unset($req['parents_id']);
        unset($req['grandparents_id']);

        $req['is_active'] = 'APPROVED';
        $req['password'] = sha1($req['password']);
        $member_id = $this->PM_Model->save($this->user_table, $req);
        if(!empty($partner_id)) {
            $this->member_model->update_relationship($member_id, $partner_id, 'PARTNER');
        }
        if(!empty($children_id)) {
            $this->member_model->update_relationship($member_id, $children_id, 'CHILDREN');
        }
        if(!empty($parents_id)) {
            $this->member_model->update_relationship($member_id, $parents_id, 'PARENT');
        }
        if(!empty($grandparents_id)) {
            $this->member_model->update_relationship($member_id, $grandparents_id, 'GRANDPARENT');
        }

        $resp['state'] = 'success';
        $resp['msg'] = 'Registered successfully!';
        $this->load_json($resp);
        return;
    }

    public function get_relationship() {

        $member_id = $this->input->post('id');

        $resp['partner'] = $this->member_model->get_relationship($member_id, 'PARTNER');
        $resp['children'] = $this->member_model->get_relationship($member_id, 'CHILDREN');
        $resp['parents'] = $this->member_model->get_relationship($member_id, 'PARENT');
        $resp['grandparents'] = $this->member_model->get_relationship($member_id, 'GRANDPARENT');

        echo json_encode($resp);
    }

    public function ajax_update_member() {
        $req = $this->input->post();
        $id = $req['id'];

        if(!isset($req['head_of_household'])) $req['head_of_household'] = 0;
        
        if(!empty($req['is_married']) && isset($req['partner_id'])) {
            $this->member_model->update_relationship($id, $req['partner_id'], 'PARTNER');
        } else {
            $req['is_married'] = 0;
            $this->member_model->update_relationship($id, array(), 'PARTNER');
        }

        if(!empty($req['has_child']) && isset($req['children_id'])) {
            $this->member_model->update_relationship($id, $req['children_id'], 'CHILDREN');
        } else {
            $req['has_child'] = 0;
            $this->member_model->update_relationship($id, array(), 'CHILDREN');
        }

        if(!empty($req['parents_id'])) {
            $this->member_model->update_relationship($id, $req['parents_id'], 'PARENT');
        } else {
            $this->member_model->update_relationship($id, array(), 'PARENT');
        }

        if(!empty($req['grandparents_id'])) {
            $this->member_model->update_relationship($id, $req['grandparents_id'], 'GRANDPARENT');
        } else {
            $this->member_model->update_relationship($id, array(), 'GRANDPARENT');
        }

        unset($req['partner_id']);
        unset($req['children_id']);
        unset($req['parents_id']);
        unset($req['grandparents_id']);

        $this->PM_Model->save($this->user_table, $req);

        $resp['state'] = 'success';
        $resp['msg'] = 'Your profile updated successfully!';
        $this->load_json($resp);
        return; 
    }
}
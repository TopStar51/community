<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Myaccount extends PM_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        
        $this->load_view('myaccount');
    }

    public function update_email() {
        $req = $this->input->post();

        $this->PM_Model->update_info($this->user_table, $req);

        $data['state'] = 'success';
        $this->load_json($data);

    }

    public function update_password() {
        $req = $this->input->post();

        $account_info = $this->PM_Model->get_info($this->user_table, $this->my_id);
        if($account_info['password'] != sha1($req['cur_pwd'])) {
            $data['state'] = 'failed';
            $data['msg'] = 'The current password is wrong!';
        } else {
            $info['password'] = sha1($req['new_pwd']);
            $info['id'] = $account_info['id'];
            $this->PM_Model->update_info($this->user_table, $info);
            $data['state'] = 'success';
        }
        $this->load_json($data);
    }

}
?>

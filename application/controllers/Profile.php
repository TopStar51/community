<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends PM_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('member_model');
	}

	public function index()
	{
		$view_params = array();
		$view_params['members'] = $this->PM_Model->get_list($this->user_table, 0, array('type'=>'MEMBER'));
		$view_params['partner'] = $this->member_model->get_relationship($this->my_id, 'PARTNER');
		$view_params['children'] = $this->member_model->get_relationship($this->my_id, 'CHILDREN');
		$view_params['parents'] = $this->member_model->get_relationship($this->my_id, 'PARENT');
		$view_params['grandparents'] = $this->member_model->get_relationship($this->my_id, 'GRANDPARENT');

		$this->load_view('profile', $view_params);
	}

	public function update_profile(){
		$req = $this->input->post();
		$id = $req['id'];

		if($req['is_married'] == "1" && isset($req['partner_id'])) {
			$this->member_model->update_relationship($id, $req['partner_id'], 'PARTNER');
		} else {
			$this->member_model->update_relationship($id, array(), 'PARTNER');
		}

		if($req['has_child'] == "1" && isset($req['children_id'])) {
			$this->member_model->update_relationship($id, $req['children_id'], 'CHILDREN');
		} else {
			$this->member_model->update_relationship($id, array(), 'CHILDREN');
		}

		if($req['has_parent'] == "1" && isset($req['parents_id'])) {
			$this->member_model->update_relationship($id, $req['parents_id'], 'PARENT');
		} else {
			$this->member_model->update_relationship($id, array(), 'PARENT');
		}

		if($req['has_gparent'] == "1" && isset($req['grandparents_id'])) {
			$this->member_model->update_relationship($id, $req['grandparents_id'], 'GRANDPARENT');
		} else {
			$this->member_model->update_relationship($id, array(), 'GRANDPARENT');
		}

		unset($req['has_parent']);
		unset($req['has_gparent']);
		unset($req['partner_id']);
		unset($req['children_id']);
		unset($req['parents_id']);
		unset($req['grandparents_id']);

		$this->PM_Model->save($this->user_table, $req);

		$resp['state'] = 'success';
		$resp['msg'] = 'Your profile updated successfully!';
		$this->load_json($resp);
		return; 
	}
}
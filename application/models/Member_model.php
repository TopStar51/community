<?php

class Member_model extends CI_Model {

	private $relation_table = "user_relationship";
	private $user_table = "users";

	function __construct(){
		parent::__construct();
	}

	function get_relationship($userid, $rel = '') {
		$this->db->select('*');
		$this->db->from($this->relation_table.' a');
		$this->db->join($this->user_table.' b', 'a.rel_id = b.id', 'left');
		$this->db->where('user_id', $userid);
		$this->db->where('b.type', 'MEMBER');
		$this->db->where('b.is_delete', '0');
		if(!empty($rel)) 
			$this->db->where('relationship', $rel);
		return $this->db->get()->result_array();
	}

	function update_relationship($user_id, $rel_ids, $rel) {
		$this->db->where(array('user_id'=>$user_id, 'relationship'=>$rel));
		$this->db->delete($this->relation_table);

		foreach ($rel_ids as $key => $rel_id) {
			$info['user_id'] = $user_id;
			$info['rel_id'] = $rel_id;
			$info['relationship'] = $rel;
			$this->db->insert($this->relation_table, $info);
		}
	}
}
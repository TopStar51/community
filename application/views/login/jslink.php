<script>
    var Login = function () {

        var handleLogin = function () {

            $('.login-form').validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true
                    },
                    remember: {
                        required: false
                    }
                },

                messages: {
                    email: {
                        required: "Email is required.",
                        email: "Email is invalid."
                    },
                    password: {
                        required: "Password is required."
                    }
                },

                invalidHandler: function (event, validator) { //display error alert on form submit
                    $('.alert-danger span', $('.login-form')).html("Enter valid email and password.");
                    $('.alert-danger', $('.login-form')).show();
                },

                highlight: function (element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                success: function (label) {
                    label.closest('.form-group').removeClass('has-error');
                    label.remove();
                },

                errorPlacement: function (error, element) {
                    error.insertAfter(element.closest('.input-icon'));
                },

                submitHandler: function (form) {
                    form.submit(); // form validation success, call ajax form submit
                }
            });

            $('.login-form input').keypress(function (e) {
                if (e.which == 13) {
                    if ($('.login-form').validate().form()) {
                        submitLogin();
                        // $('.login-form').submit(); //form validation success, call ajax form submit
                    }
                    return false;
                }
            });

            $('.btn-login').click(function() {
                if ($('.login-form').validate().form()) {
                    submitLogin();
                }
            });
        }

        var handleForgetPassword = function() {
            $('.forget-form').validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                    email: {
                        required: true,
                        email: true
                    }
                },

                messages: {
                    email: {
                        required: "Email is required."
                    }
                },

                invalidHandler: function(event, validator) { //display error alert on form submit   

                },

                highlight: function(element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                success: function(label) {
                    label.closest('.form-group').removeClass('has-error');
                    label.remove();
                },

                errorPlacement: function(error, element) {
                    error.insertAfter(element.closest('.input-icon'));
                },

                submitHandler: function(form) {
                    form.submit();
                }
            });

            $('.forget-form input').keypress(function(e) {
                if (e.which == 13) {
                    if ($('.forget-form').validate().form()) {
                        submitForgetpwd();
                    }
                    return false;
                }
            });

            $('.btn-reset-pwd').click(function() {
                if ($('.forget-form').validate().form()) {
                    submitForgetpwd();
                }
            })

            jQuery('#forget-password').click(function() {
                jQuery('.login-form').hide();
                jQuery('.forget-form').show();
            });

            jQuery('#back-btn').click(function() {
                jQuery('.login-form').show();
                jQuery('.forget-form').hide();
            });

        }

        var handleRegister = function() {

            $('.register-form').validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "",
                rules: {
                    first_name: {
                        required: true
                    },
                    last_name: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true
                    },
                    rpassword: {
                        equalTo: "#register_password"
                    },
                },

                /*messages: { // custom messages for radio buttons and checkboxes
                    tnc: {
                        required: "Please accept TNC first."
                    }
                },*/

                invalidHandler: function(event, validator) { //display error alert on form submit   

                },

                highlight: function(element) { // hightlight error inputs
                    $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                success: function(label) {
                    label.closest('.form-group').removeClass('has-error');
                    label.remove();
                },

                errorPlacement: function(error, element) {
                    if (element.attr("name") == "tnc") { // insert checkbox errors after the container                  
                        error.insertAfter($('#register_tnc_error'));
                    } else if (element.closest('.input-icon').size() === 1) {
                        error.insertAfter(element.closest('.input-icon'));
                    } else {
                        error.insertAfter(element);
                    }
                },

                submitHandler: function(form) {
                    form.submit();
                }
            });

            $('.register-form input').keypress(function(e) {
                if (e.which == 13) {
                    if ($('.register-form').validate().form()) {
                        submitRegister();
                    }
                    return false;
                }
            });

            $("#register-submit-btn").click(function(){
                if ($('.register-form').validate().form()) {                   
                    submitRegister();
                }
            });

            jQuery('#register-btn').click(function() {
                jQuery('.login-form').hide();
                jQuery('.register-form').show();
            });

            jQuery('#register-back-btn').click(function() {
                jQuery('.login-form').show();
                jQuery('.register-form').hide();
            });
        }

        var submitLogin = function() {
            $.ajax({
                type: 'POST',
                url: "<?php echo site_url('login/ajax_login'); ?>",
                data: $('.login-form').serializeArray(),
                dataType: 'json',
                success: function (resp) {
                    if (resp.state == 'success') {
                        window.location.href = resp.url;
                    } else {
                        $('.alert-danger span', $('.login-form')).html(resp.message);
                        $('.alert-danger', $('.login-form')).show();
                    }
                },
                error: function () {
                    $('.alert-danger span', $('.login-form')).html("Error occured.");
                    $('.alert-danger', $('.login-form')).show();
                }
            });
        }

        var submitForgetpwd = function() {
            $.ajax({
                type: 'POST',
                url: "<?php echo site_url('login/ajax_forget_pwd'); ?>",
                data: $('.forget-form').serializeArray(),
                dataType: 'json',
                success: function (resp) {
                    if (resp.state == 'success') {
                        $('.login-form').show();
                        $('.forget-form').hide();
                        $('.alert-success span', $('.login-form')).html(resp.message);
                        $('.alert-success', $('.login-form')).show();
                    } else {
                        $('.alert-danger span', $('.forget-form')).html(resp.message);
                        $('.alert-danger', $('.forget-form')).show();
                    }
                },
                error: function () {
                    $('.alert-danger span', $('.forget-form')).html("Error occured.");
                    $('.alert-danger', $('.forget-form')).show();
                }
            })
        }

        var submitRegister = function() {
            $.ajax({
                type: 'POST',
                url: "<?php echo site_url('login/ajax_register'); ?>",
                data: $('.register-form').serializeArray(),
                dataType: 'json',
                success: function (resp) {
                    console.log(resp);
                    if (resp.state == 'success') {
                        window.location.href = resp.url;
                    } else {
                        $('.alert-danger span', $('.register-form')).html(resp.message);
                        $('.alert-danger', $('.register-form')).show();
                    }
                },
                error: function () {
                    $('.alert-danger span', $('.register-form')).html("Error occured.");
                    $('.alert-danger', $('.register-form')).show();
                }
            });
        }

        return {
            //main function to initiate the module
            init: function () {

                handleLogin();
                handleForgetPassword();
                handleRegister();
                // init background slide images
                $.backstretch([
                        "<?=base_url('assets')?>/custom/img/login/login-1.jpg"
                    ], {
                        fade: 1000,
                        duration: 8000
                    }
                );

            }

        };

    }();

    jQuery(document).ready(function () {
        Login.init();
    });
</script>
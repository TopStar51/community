<script type="text/javascript">

var FormValidation = function () {

    // basic validation
    var handleProfileValidation = function() {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation

        var form = $('#form_member');
        var error = $('.alert-danger', form);
        var success = $('.alert-success', form);
        var neterror = $('.alert-warning', form);

        form.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            messages: {
                select_multi: {
                    maxlength: jQuery.validator.format("Max {0} items allowed for selection"),
                    minlength: jQuery.validator.format("At least {0} items must be selected")
                }
            },
            rules: {
                first_name: {
                    required: true
                },
                last_name: {
                    required: true
                },
                email: {
                    required: true
                },
                password: {
                    minlength: 6,
                    required: true
                },
                rpassword: {
                    required: true,
                    minlength: 6,
                    equalTo: '#password',
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit              
                success.hide();
                error.show();
                App.scrollTo(error, -200);
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                var cont = $(element).parent('.input-group');
                if (cont.size() > 0) {
                    cont.after(error);
                } else {
                    element.after(error);
                }
            },

            highlight: function (element) { // hightlight error inputs

                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },

            submitHandler: function (form) {
                error.hide();
                neterror.hide();
                process_ajax('',
                    '<?=site_url('member/add_member')?>',
                    $(form).serializeArray(),
                    function (resp) {
                        show_toastr('success', '', resp.msg);
                        setTimeout(function() {
                            window.location.href = "<?=site_url('member/index')?>";
                        }, 1000);
                    },
                    function (resp) {
                        show_toastr('danger', '', resp.msg);
                        //neterror.show();
                    }
                );
            }
        });
    }

    return {
        //main function to initiate the module
        init: function () {

            handleProfileValidation();

        }

    };

}();

jQuery(document).ready(function() {
    FormValidation.init();
    
    $("#birthday, #parents_yartzheit, #children_yartzheit, #sibling_yartzheit, #grandparents_yartzheit").inputmask("d/m/y", {
        "placeholder": "dd/mm/yyyy",
    }); 

    $.fn.select2.defaults.set("theme", "bootstrap");

    $("#gender").select2({
        placeholder: "Select a gender",
        width: null
    });

    $("#country").select2({
        placeholder: "Select a country",
        width: null
    });

    $("#partner_id").select2({
        placeholder: "Select a partner",
        width: null
    });

    $("#children_id").select2({
        placeholder: "Select children",
        width: null
    });

    $("#parents_id").select2({
        placeholder: "Select parents",
        width: null
    });

    $("#grandparents_id").select2({
        placeholder: "Select grandparents",
        width: null
    });

    $("#is_married").click(function(){
        if($(this).prop('checked')) {
            $("#div-partner").show();
        }
        else {
            $("#div-partner").hide();
        }
    });

    $("#has_child").click(function(){
        if($(this).prop('checked')) {
            $("#div-children").show();
        }
        else {
            $("#div-children").hide();
        }
    });

});
</script>
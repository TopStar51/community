<script>
    var TableDatatablesAjax = function () {

        var handleTable = function () {

            var table = $('#datatable_ajax');

            var grid = new Datatable();

            grid.init({
                src: $("#datatable_ajax"),
                onSuccess: function (grid, response) {
                    // grid:        grid object
                    // response:    json object of server side ajax response
                    // execute some code after table records loaded
                },
                onError: function (grid) {
                    // execute some code on network or other general error
                },
                onDataLoad: function(grid) {
                    // execute some code on ajax data load
                },
                loadingMessage: 'Loading...',
                dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options

                    // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                    // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                    // So when dropdowns used the scrollable div should be removed.
                    //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                    // save datatable state(pagination, sort, etc) in cookie.
                    "bStateSave": true,

                    // save custom filters to the state
                    "fnStateSaveParams":   function ( oSettings, sValue ) {
                        $("#datatable_ajax tr.filter .form-control").each(function() {
                            sValue[$(this).attr('name')] = $(this).val();
                        });

                        return sValue;
                    },

                    // read the custom filters from saved state and populate the filter inputs
                    "fnStateLoadParams" : function ( oSettings, oData ) {
                        //Load custom filters
                        $("#datatable_ajax tr.filter .form-control").each(function() {
                            var element = $(this);
                            if (oData[element.attr('name')]) {
                                element.val( oData[element.attr('name')] );
                            }
                        });

                        return true;
                    },

                    "lengthMenu": [
                        [10, 20, 50, 100, 150, -1],
                        [10, 20, 50, 100, 150, "All"] // change per page values here
                    ],
                    "pageLength": 20, // default record count per page
                    "ajax": {
                        "url": "<?php echo site_url('member/ajax_get_members'); ?>", // ajax source
                    },
                    "ordering": false,
                    "serverSide": false,
                    "order": [
                        [1, "asc"]
                    ]// set first column as a default sort by asc
                }
            });

            table.on('click', '.edit', function (e) {
                e.preventDefault();

                var nRow = $(this).parents('tr')[0];
                var first_name = $('.user-first_name', nRow).val();
                var last_name = $('.user-last_name', nRow).val();
                var birthday = $('.user-birthday', nRow).val();
                var gender = $('.user-gender', nRow).val();
                var address1 = $('.user-address1', nRow).val();
                var address2 = $('.user-address2', nRow).val();
                var city = $('.user-city', nRow).val();
                var state = $('.user-state', nRow).val();
                var zip = $('.user-zip', nRow).val();
                var country = $('.user-country', nRow).val();
                var cell = $('.user-cell_phone', nRow).val();
                var home = $('.user-home_phone', nRow).val();
                var email = $('.user-email', nRow).val();
                var head_of_household = $('.user-head_of_household', nRow).val();
                var is_married = $('.user-is_married', nRow).val();
                var has_child = $('.user-has_child', nRow).val();
                var id = $('.user-id', nRow).val();
                var is_active = $('.user-is_active', nRow).val();
                var partner = [], children = [], parents = [], grandparents = [];

                $.ajax({
                    url : '<?=site_url('member/get_relationship')?>',
                    method : 'POST',
                    data : {id : id},
                    dataType: 'json',
                    success: function(resp){
                        for(var i=0 ; i<resp.partner.length; i++) {
                            partner.push(resp.partner[i]['id']);
                        }
                        $('#partner_id').val(partner).change();
                        for(var i=0 ; i<resp.children.length; i++) {
                            children.push(resp.children[i]['id']);
                        }
                        $('#children_id').val(children).change();
                        for(var i=0 ; i<resp.parents.length; i++) {
                            parents.push(resp.parents[i]['id']);
                        }
                        $('#parents_id').val(parents).change();
                        for(var i=0 ; i<resp.grandparents.length; i++) {
                            grandparents.push(resp.grandparents[i]['id']);
                        }
                        $('#grandparents_id').val(grandparents).change();
                    }
                });

                $('#user_id').val(id);
                $('#first_name').val(first_name);
                $('#last_name').val(last_name);
                $('#birthday').val(birthday);
                $("#gender").val(gender).change();
                $('#address1').val(address1);
                $('#address2').val(address2);
                $('#city').val(city);
                $('#state').val(state);
                $('#zip').val(zip);
                $("#country").val(country).change();
                $('#cell_phone').val(cell);
                $('#home_phone').val(home);
                $('#email').val(email);
                $("#is_active").val(is_active).change();

                if(head_of_household == "1") 
                    $("#head_of_household", "label.mt-checkbox").prop('checked', true);

                if(is_married == "1") {
                    $("#is_married", "label.mt-checkbox").prop('checked', true);
                    $("#div-partner").show();
                }

                $("#is_married", "label.mt-checkbox").click(function(){
                    if($(this).prop('checked')) {
                        $("#div-partner").show();
                    }
                    else {
                        $("#div-partner").hide();
                    }
                });

                if(has_child == "1") {
                    $("#has_child", "label.mt-checkbox").prop('checked', true);
                    $("#div-children").show();
                }

                $("#has_child", "label.mt-checkbox").click(function(){
                    if($(this).prop('checked')) {
                        $("#div-children").show();
                    }
                    else {
                        $("#div-children").hide();
                    }
                });

                $('#m_user_edit_modal').modal('show');
            });

            table.on('click', '.delete', function (e) {
                e.preventDefault();

                var nRow = $(this).parents('tr')[0];
                var user_id = $('.user-id', nRow).val();

                if (confirm('Do you really delete this user?')) {
                    process_ajax('',
                        '<?=site_url('member/ajax_delete_member')?>',
                        {
                            id: user_id
                        },
                        function (resp) {
                            show_toastr("success", '', 'Deleted!');
                            setTimeout(function () {
                                location.reload();
                            }, 600);
                        },
                        function (resp) {
                            show_toastr('warning', 'Failed');
                        }
                    );
                }
            });

            /*table.on('click', 'tbody tr', function (e) {
                e.preventDefault();

                if ($(e.target).hasClass('edit') || $(e.target).hasClass('delete') || $(e.target).children('.edit').length)
                    return;

                if ($(this).children().length == 1)
                    return;

                var nRow = $(this);
                var input_name = $('.user-name', nRow).val();

                $('#info_name').val(input_name);

                $('#m_user_info_modal').modal('show');
            })*/

            $('thead').find('.filter').find('input').on('keyup', function(e) {
                if (e.keyCode == 13) {
                    grid.submitFilter();
                }
            });

            $('thead').find('.filter').find('select').on('change', function () {
                grid.submitFilter();
            });

            //grid.setAjaxParam("customActionType", "group_action");
            //grid.getDataTable().ajax.reload();
            //grid.clearAjaxParams();
        }

        var editFormInit = function () {
            $('#form_edit_member').validate({
                // define validation rules
                rules: {
                    //=== Client Information(step 3)
                    //== Billing Information
                    first_name: {
                        required: true
                    },
                    last_name: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    }
                },

                //display error alert on form submit
                invalidHandler: function(event, validator) {
                    swal("There are some errors in your submission. Please correct them.");
                },

                submitHandler: function (form) {
                    process_ajax('',
                        '<?=site_url('member/ajax_update_member')?>',
                        $(form).serializeArray(),
                        function (resp) {
                            show_toastr('success', '', resp.msg);
                            setTimeout(function() {
                                location.reload();
                            }, 1000);
                        },
                        function (resp) {
                            show_toastr('danger', '', resp.msg);
                        }
                    );

                    return false;
                }
            });
        }

        return {

            //main function to initiate the module
            init: function () {

                handleTable();
                editFormInit();
            }

        };

    }();

    jQuery(document).ready(function() {

        $("#birthday, #parents_yartzheit, #children_yartzheit, #sibling_yartzheit, #grandparents_yartzheit").inputmask("d/m/y", {
            "placeholder": "dd/mm/yyyy",
        }); 
        
        $('#gender').select2({
            placeholder: "Select a gender",
            width: null
        });

        $("#country").select2({
            placeholder: "Select a country",
            width: null
        });

        $("#partner_id").select2({
            placeholder: "Select a partner",
            width: null
        });

        $("#children_id").select2({
            placeholder: "Select children",
            width: null
        });

        $("#parents_id").select2({
            placeholder: "Select parents",
            width: null
        });

        $("#grandparents_id").select2({
            placeholder: "Select grandparents",
            width: null
        });

        TableDatatablesAjax.init();
    });
</script>
<!-- BEGIN PAGE BAR -->
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="#">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#"> account </a>
        </li>
    </ul>
</div>
<!-- END PAGE BAR -->
<div class="row">
    <div class="col-md-12">
        <div class="portlet light ">
            <div class="portlet-title tabbable-line">
                <div class="caption caption-md">
                    <i class="icon-globe theme-font hide"></i>
                    <span class="caption-subject font-blue-madison bold">My Account</span>
                </div>
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#personal_info" data-toggle="tab">Change Email</a>
                    </li>
                    <li>
                        <a href="#change_password" data-toggle="tab">Change Password</a>
                    </li>
                </ul>
            </div>
            <div class="portlet-body">
                <div class="tab-content">
                    <!-- PERSONAL INFO TAB -->
                    <div class="tab-pane active" id="personal_info">
                        <form role="form" id="frm_personal_info">
                            <input hidden name="id" value="<?=$user_data['id']?>">
                            <div class="form-group">
                                <label class="control-label">Email</label>
                                <input type="email" placeholder="example@email.com" class="form-control" id="email" name="email" value="<?=$user_data['email']?>" required/> </div>
                            <div class="margiv-top-10">
                                <a class="btn green" id="btn_save_email"> Save Changes </a>
                            </div>
                        </form>
                    </div>
                    <!-- END PERSONAL INFO TAB -->
                    <!-- CHANGE PASSWORD TAB -->
                    <div class="tab-pane" id="change_password">
                        <form id="frm_change_pwd">
                            <div class="form-group">
                                <label class="control-label">Current Password</label>
                                <input type="password" class="form-control" name="cur_pwd" /> </div>
                            <div class="form-group">
                                <label class="control-label">New Password</label>
                                <input type="password" class="form-control" name="new_pwd" id="new_pwd" /> </div>
                            <div class="form-group">
                                <label class="control-label">Re-type New Password</label>
                                <input type="password" class="form-control" name="confirm_pwd" /> </div>
                            <div class="margin-top-10">
                                <a id="btn_save_pwd" class="btn green"> Change Password </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){

        $("#frm_change_pwd").validate({
            rules: {
                cur_pwd: {
                    required: true
                },
                new_pwd: {
                    minlength: 6,
                    required: true
                },
                confirm_pwd: {
                    required: true,
                    minlength: 6,
                    equalTo: '#new_pwd',
                }
            }
        });

        $('#btn_save_email').click(function(){
            var email = $('#email').val();
            if(email == '' || email == undefined) {
                show_toastr("error", '', 'Email is required');
                return;
            }
            $.ajax({
                url: '<?php echo site_url("Myaccount/update_email");?>',
                method: 'POST',
                data: $("#frm_personal_info").serializeArray(),
                dataType: 'json',
                success: function(resp){
                    if (resp.state == 'success') {
                        show_toastr("success", '', 'Updated successfully');
                    }
                    else {
                        show_toastr("error", '', 'Failed to update email');
                    }
                }
            })
        });

        $("#btn_save_pwd").click(function(){
            if($("#frm_change_pwd").valid()){
                $.ajax({
                    url: '<?php echo site_url("Myaccount/update_password");?>',
                    method: 'POST',
                    data: $("#frm_change_pwd").serializeArray(),
                    dataType: 'json',
                    success: function(resp){
                        if (resp.state == 'success') {
                            show_toastr("success", '', 'Updated successfully');
                        }
                        else {
                            show_toastr("error", resp.msg, 'Failed to change password');
                        }
                    }
                })
            }
        });
    });

</script>
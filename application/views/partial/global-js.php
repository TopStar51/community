<script>

    var process_ajax = function (blockDiv, url, data, successCallback, errorCallback) {
        if (!blockDiv && blockDiv != '') $('#' + blockDiv).prop('disabled', true);
        $(document.body).css({'cursor' : 'wait'});
        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'json',
            data: data,
            success: function (resp) {
                $(document.body).css({'cursor' : 'default'});
                if (!blockDiv && blockDiv != '') $('#' + blockDiv).prop('disabled', false);
                if (resp.state == 'success') {
                    successCallback(resp);
                } else {
                    errorCallback(resp);
                }
            },
            error: function () {
                errorCallback('Error occured.');
                $(document.body).css({'cursor' : 'default'});
            }
        });
    };

    // Functions made by Alex

    var process_form_data = function (blockDiv, url, data, successCallback, errorCallback) {
        if (!blockDiv && blockDiv != '') $('#' + blockDiv).prop('disabled', true);
        $(document.body).css({'cursor' : 'wait'});

        var request = new XMLHttpRequest();
        request.open("POST", url);

        request.onload = function() {
            var resp;
            resp = JSON.parse(request.responseText);

            $(document.body).css({'cursor' : 'default'});
            if (!blockDiv && blockDiv != '') $('#' + blockDiv).prop('disabled', false);

            if (resp.state == 'success') {
                successCallback(resp);
            } else {
                errorCallback(resp);
            }
            return;
        };
        request.send(data);
    };

    //Functions End

    var show_toastr = function($type, $msg, $title) {
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "2000",
            "hideDuration": "1000",
            "timeOut": "3000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        toastr[$type]($msg, $title);
    };

    function validate_email(sEmail) {
        var filter = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (filter.test(sEmail)) {
            return true;
        }
        else {
            return false;
        }
    }

</script>

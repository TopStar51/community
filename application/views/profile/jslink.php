<script type="text/javascript">

var head_of_household = "<?=$user_data['head_of_household']?>";
var is_married = "<?=$user_data['is_married']?>";
var has_child = "<?=$user_data['has_child']?>";

var parents = [];
<?php foreach($parents as $parent) { ?>
    parents.push("<?=$parent['rel_id']?>");
<?php } ?>

var children = [];
<?php foreach($children as $child) { ?>
    children.push("<?=$child['rel_id']?>");
<?php } ?>

var grandparents = [];
<?php foreach($grandparents as $grandparent) { ?>
    grandparents.push("<?=$grandparent['rel_id']?>");
<?php } ?>

var FormValidation = function () {

    // basic validation
    var handleProfileValidation = function() {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation

        var form = $('#form_profile');
        var error = $('.alert-danger', form);
        var success = $('.alert-success', form);
        var neterror = $('.alert-warning', form);

        form.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            messages: {
                select_multi: {
                    maxlength: jQuery.validator.format("Max {0} items allowed for selection"),
                    minlength: jQuery.validator.format("At least {0} items must be selected")
                }
            },
            rules: {
                first_name: {
                    required: true
                },
                last_name: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                }
            },

            invalidHandler: function (event, validator) { //display error alert on form submit              
                success.hide();
                error.show();
                App.scrollTo(error, -200);
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                var cont = $(element).parent('.input-group');
                if (cont.size() > 0) {
                    cont.after(error);
                } else {
                    element.after(error);
                }
            },

            highlight: function (element) { // hightlight error inputs

                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },

            submitHandler: function (form) {
                error.hide();
                neterror.hide();
                process_ajax('',
                    '<?=site_url('profile/update_profile')?>',
                    $(form).serializeArray(),
                    function (resp) {
                        show_toastr('success', '', resp.msg);
                        /*setTimeout(function() {
                            location.reload();
                        }, 500);*/
                    },
                    function (resp) {
                        neterror.show();
                    }
                );
            }
        });
    }

    return {
        //main function to initiate the module
        init: function () {

            handleProfileValidation();

        }

    };

}();

jQuery(document).ready(function() {

    FormValidation.init();

    $("#country").val("<?=$user_data['country']?>");
    
    $("#gender").val("<?=$user_data['gender']?>");

    $("#parents_id").val(parents);

    $("#children_id").val(children);

    $("#grandparents_id").val(grandparents);

    if(head_of_household == "1") {
        $("#head_of_household1").prop('checked', true);
    }

    if(is_married == "1") {
        $("#is_married").prop('checked', true);
        $("#div-partner").show();
    }

    $("input[name='is_married']").change(function(){
        if($(this).val() == "1") {
            $("#div-partner").show();
            $("#is_married").prop('checked', true);
        }
        else {
            $("#div-partner").hide();
            $("#not_married").prop('checked', true);
        }
    });

    if(has_child == "1") {
        $("#has_child").prop('checked', true);
        $("#div-children").show();
    }

    $("input[name='has_child']").change(function(){
        if($(this).val() == "1") {
            $("#div-children").show();
            $("#has_child").prop('checked', true);
        }
        else {
            $("#div-children").hide();
            $("#has_not_child").prop('checked', true);
        }
    });

    if(parents.length > 0) {
        $("#has_parent").prop('checked', true);
        $("#div-parent").show();
    }

    $("input[name='has_parent']").change(function(){
        if($(this).val() == "1") {
            $("#div-parent").show();
            $("#has_parent").prop('checked', true);
        }
        else {
            $("#div-parent").hide();
            $("#has_not_parent").prop('checked', true);
        }
    });

    if(grandparents.length > 0) {
        $("#has_gparent").prop('checked', true);
        $("#div-gparent").show();
    }

    $("input[name='has_gparent']").change(function(){
        if($(this).val() == "1") {
            $("#div-gparent").show();
            $("#has_gparent").prop('checked', true);
        }
        else {
            $("#div-gparent").hide();
            $("#has_not_gparent").prop('checked', true);
        }
    });

    $("#birthday").inputmask("d/m/y", {
        "placeholder": "dd/mm/yyyy"
    }); 

    $.fn.select2.defaults.set("theme", "bootstrap");

    $("#country").select2({
        placeholder: "Select a country",
        width: null
    });



    $("#gender").select2({
        placeholder: "Select a gender",
        width: null
    })

    $("#birthday, #parents_yartzheit, #children_yartzheit, #sibling_yartzheit, #grandparents_yartzheit").inputmask("d/m/y", {
        "placeholder": "dd/mm/yyyy",
    }); 

    $.fn.select2.defaults.set("theme", "bootstrap");

    $("#partner_id").select2({
        placeholder: "Select a partner",
        width: null
    });

    $("#children_id").select2({
        placeholder: "Select children",
        width: null
    });

    $("#parents_id").select2({
        placeholder: "Select parents",
        width: null
    });

    $("#grandparents_id").select2({
        placeholder: "Select grandparents",
        width: null
    });
});
</script>
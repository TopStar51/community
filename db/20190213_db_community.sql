/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 100130
Source Host           : localhost:3306
Source Database       : db_community

Target Server Type    : MYSQL
Target Server Version : 100130
File Encoding         : 65001

Date: 2019-02-13 22:03:44
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `type` enum('ADMIN','MEMBER') NOT NULL DEFAULT 'MEMBER',
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `gender` enum('Male','Female') DEFAULT 'Male',
  `birthday` varchar(100) DEFAULT NULL,
  `address1` varchar(100) DEFAULT NULL,
  `address2` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `zip` varchar(255) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `cell_phone` varchar(100) DEFAULT NULL,
  `home_phone` varchar(255) DEFAULT NULL,
  `head_of_household` tinyint(1) DEFAULT '0',
  `is_married` tinyint(1) DEFAULT '0',
  `parents_yartzheit` varchar(100) DEFAULT NULL,
  `children_yartzheit` varchar(100) DEFAULT NULL,
  `sibling_yartzheit` varchar(100) DEFAULT NULL,
  `grandparents_yartzheit` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `is_delete` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'admin@email.com', '3d4f2bf07dc1be38b20cd6e46949a1071f9d0e3d', 'ADMIN', 'James', 'Rodrigo', 'Male', '11/02/1985', null, null, null, null, null, 'US', null, null, '1', '1', null, null, null, null, '2019-02-12 02:17:13', '2019-02-12 14:38:30', '0');
INSERT INTO `users` VALUES ('2', 'juan@email.com', '011c945f30ce2cbafc452f39840f025693339c42', 'MEMBER', 'Juan', 'Gonzalez', 'Male', '01/11/1990', '2739 Joshua Creek Rd', null, 'Chula Vista ', 'CA', '91914', 'US', null, null, '1', '1', null, null, null, null, '2019-02-12 19:10:53', '2019-02-12 19:10:55', '0');
INSERT INTO `users` VALUES ('3', 'pancha@email.com', '011c945f30ce2cbafc452f39840f025693339c42', 'MEMBER', 'Pancha', 'Lopez', 'Female', '25/05/1990', '2739 Joshua Creek Rd', null, 'Chula Vista ', 'CA', '91914', 'US', null, null, '0', '1', null, null, null, null, '2019-02-12 19:14:00', '2019-02-12 19:14:02', '0');
INSERT INTO `users` VALUES ('4', 'john@email.com', '011c945f30ce2cbafc452f39840f025693339c42', 'MEMBER', 'John', 'Stones', 'Male', '21/02/1960', '2739 Joshua Creek Rd', null, 'Chula Vista ', 'CA', '91914', 'US', null, null, '0', '1', null, null, null, null, '2019-02-13 13:35:06', '2019-02-13 13:35:09', '0');
INSERT INTO `users` VALUES ('5', 'topstar51@outlook.com', '011c945f30ce2cbafc452f39840f025693339c42', 'MEMBER', 'top', 'star', 'Male', null, null, null, null, null, null, null, null, null, '0', '0', null, null, null, null, '2019-02-13 14:56:27', '2019-02-13 14:56:27', '0');
INSERT INTO `users` VALUES ('6', 'test@email.com', '011c945f30ce2cbafc452f39840f025693339c42', 'MEMBER', 'test', 'user', 'Male', null, null, null, null, null, null, null, null, null, '0', '0', null, null, null, null, '2019-02-13 15:03:03', '2019-02-13 15:03:03', '0');
INSERT INTO `users` VALUES ('7', 'lazar@outlook.com', '011c945f30ce2cbafc452f39840f025693339c42', 'MEMBER', 'Lazar', 'Kolarov', 'Male', null, null, null, null, null, null, null, null, null, '0', '0', null, null, null, null, '2019-02-13 15:30:00', '2019-02-13 15:30:00', '0');
INSERT INTO `users` VALUES ('8', 'test123@outlook.com', '011c945f30ce2cbafc452f39840f025693339c42', 'MEMBER', 'test', 'member2', 'Male', null, null, null, null, null, null, null, null, null, '0', '0', null, null, null, null, '2019-02-13 15:58:44', '2019-02-13 15:58:44', '0');

-- ----------------------------
-- Table structure for user_relationship
-- ----------------------------
DROP TABLE IF EXISTS `user_relationship`;
CREATE TABLE `user_relationship` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `rel_id` int(11) NOT NULL,
  `relationship` enum('PARTNER','CHILDREN','PARENT','GRANDPARENT') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_relationship
-- ----------------------------
INSERT INTO `user_relationship` VALUES ('1', '2', '3', 'PARTNER');
INSERT INTO `user_relationship` VALUES ('2', '2', '4', 'PARENT');
